#include <iostream>

using std::cout;
using std::endl;

//Testing the differences between char[] and char*[]

//Declare some chars to use with char*[]
char cc = 'c';
char ch = 'h';
char ca = 'a';
char cr = 'r';

int main() {
    char cpa[] = {'c', 'h', 'a', 'r'};
    //Array of char pointers; in other words:
    // A char pointer pointing to the first pointer
    // in the array
    char* cppa[] = {&cc, &ch, &ca, &cr};

    cout << "\nchar array..." << endl;
    for(char c : cpa) {
        cout << c << ' ';
    } cout << endl;

    cout << "\nchar* array..." << endl;
    for(char* c: cppa) {
        cout << *c << ' ';
    } cout << endl;
    
    return EXIT_SUCCESS;
}