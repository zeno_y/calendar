#include <sstream> //std::istringstream. Converts console-in string into int
#include "calendar.hpp"
#include "usage.hpp"

using namespace calendar;
using namespace usage;

//Prints out a Gregorian calendar for the current year/month
// Indentifies which day it is.

/**
 * Main function.
 * Runs the program.
*/
int main(int argc, char * argv[]) {
    //Determines current date/time
    time_t current = time(0);
    //Creates a structure from the time_t previously defined
    tm * tm_struct = localtime(&current);

    //Determines whether current day should be indicated.
    // This occurs only when no arguments are provided
    bool indicateCurrent = false;
    //Determines whether time should be printed.
    // Set to false if there's at least one argument
    bool indicateTime = true;
    //First arg is program name
    // Check for args, make adjustment to tm_struct accordingly
    if(argc > 1) {
        //Determines whether an invalid argument was provided
        bool argNotValid = true;
        //Prevents time from being printed
        indicateTime = false;
        //Loop through args. Index is also incremented within
        // the IF blocks.
        for(int i = 1; i < argc; ++i) {
            //Checks for help argument -h
            // Is ignored if it's not the first argument seen
            // and if it's not the only argument provided.
            if(strncmp(ARG_HELP, argv[i], ARG_SIZE) == 0) {
                if(i == 1 && argc == 2) {
                    print_usage();

                    return EXIT_SUCCESS;
                } else continue; //skips code below for current iteration of loop
            }

            //Checks for month argument -m
            // Sets the time to display calendar for the argument value
            if(strncmp(ARG_MONTH, argv[i], ARG_SIZE) == 0) {
                //Checks if arg is the last entry in argv
                if(i == argc - 1) {
                    std::cout << "No month value provided!\n" << std::endl;

                    print_usage();
                    return EXIT_FAILURE;
                }

                //Entering this IF block means the argument is valid
                argNotValid = false;
                //Will be true if the flag was set to true within the loop
                indicateCurrent = indicateCurrent || false;

                //Retrieves value from console in; converts
                std::istringstream month_str(argv[++i]);
                unsigned int month = 0;
                month_str >> month;

                //Checks whether month value is valid (between 1 - 12)
                if(month < 1 || month > 12) {
                    std::cout << "Provide a month value between 1 - 12.\n" << std::endl;

                    print_usage();
                    return EXIT_FAILURE;
                }
                
                //Subracts one from month to match format of tm_mon (0 - 11)
                tm_struct->tm_mon = --month;
            }

            //Checks for year argument -y
            // Sets the time to display calendar for the argument value
            if(strncmp(ARG_YEAR, argv[i], ARG_SIZE) == 0) {
                //Checks if arg is the last entry in argv
                if(i == argc - 1) {
                    std::cout << "No year value provided!\n" << std::endl;

                    print_usage();
                    return EXIT_FAILURE;
                }

                //Entering this IF block means the argument is valid
                argNotValid = false;
                //Will be true if the flag was set to true within the loop
                indicateCurrent = indicateCurrent || false;

                //Retrieves value from console in; converts
                std::istringstream year_str(argv[++i]);
                unsigned int year = 0;
                year_str >> year;

                // Must be larger than 1900
                if(year < 1900 || year > 9999) {
                    std::cout << "Provide a year value between 1900 - 9999.\n" << std::endl;
                    
                    print_usage();
                    return EXIT_FAILURE;
                }

                //Subtracts 1900 from year to match format of tm_year (0 - XX)
                tm_struct->tm_year = sub_year(year);
            }

            //Checks for day argument -d
            // Sets the time to display calendar for the argument value
            if(strncmp(ARG_DAY, argv[i], ARG_SIZE) == 0) {
                //Checks if arg is the last entry in argv
                if(i == argc - 1) {
                    std::cout << "No day value provided!\n" << std::endl;

                    print_usage();
                    return EXIT_FAILURE;
                }
                
                //Entering this IF block means the argument is valid
                argNotValid = false;
                //Should indicate current day if one of the passed arguments
                // is ARG_DAY
                indicateCurrent = true;

                //Retrieves value from console in; converts
                std::istringstream day_str(argv[++i]);
                unsigned int day = 0;
                day_str >> day;

                //Day must be within days in month of given month
                if(!is_in_month(day, tm_struct)) {
                    std::cout << "Not a valid day for month of ";
                    std::cout << MONTHS[tm_struct->tm_mon].first << '.' << '\n' << std::endl;

                    print_usage();
                    return EXIT_FAILURE;
                }

                tm_struct->tm_mday = day;
            }

            //Checks if arguments are NOT valid. Exits program if true
            if(argNotValid) {
                std::cout << "One of the arguments were not valid." << std::endl;

                print_usage();
                return EXIT_FAILURE;
            }

            //Revises time struct
            time_t adjusted = mktime(tm_struct);
            tm_struct = localtime(&adjusted);
        }
    //Indicates current day if no arguments are provided
    } else indicateCurrent = true;

    //Declares a pointer to current month
    const month_t * currMonth = &MONTHS[tm_struct->tm_mon];

    //Prints the top line using month and year
    print_month_year(currMonth->first, add_year(tm_struct->tm_year), is_lyear(tm_struct->tm_year));
    //Prints second line using hour, min, secs
    if(indicateTime) print_time(tm_struct->tm_hour, tm_struct->tm_min, tm_struct->tm_sec);
    //Print Sunday - Monday
    print_days();

    //Adjusts days in month (increments by one) if it's February of a leap year.
    const int daysInMonth = currMonth->second + 
                                (is_lyear(tm_struct->tm_year) && tm_struct->tm_mon == 1 ? 1 : 0);

    //Prints digit of month
    print_digits(
        first_of_month(tm_struct->tm_mday, tm_struct->tm_wday),
        tm_struct->tm_mday, daysInMonth, indicateCurrent
    );

    return EXIT_SUCCESS; //Success
}