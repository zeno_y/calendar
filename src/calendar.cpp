#include "calendar.hpp"

using std::cout;
using std::endl;
using std::min;

#define END '|'
#define LIN '='
#define SP ' '
#define CL ':'

#define IND_L '<'
#define IND_R '>'

//Number of LIN chars to print
#define LIN_C 5
//Number of weekdays
#define DAYS_IN_WEEK 7
//Number of digits per weekday print
#define DIGITS_PER_DAY 3

//An array of days of week
const char * calendar::DAYS[] = {
    "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"
};

//An array of possible days of month
const char * calendar::DIGITS[] = {
    "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
    "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
    "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
    "31"
};

//An array of month_t values.
const calendar::month_t calendar::MONTHS[] = {
    {"Jan", 31}, {"Feb", 28}, {"Mar", 31}, {"Apr", 30}, {"May", 31}, {"Jun", 30},
    {"Jul", 31}, {"Aug", 31}, {"Sep", 30}, {"Oct", 31}, {"Nov", 30}, {"Dec", 31}
};

/**
 * Determines the year return from tm_year.
*/
int calendar::add_year(const int y_count) {
    return 1900 + y_count;
}

/**
 * Determines the year count for tm_year
*/
int calendar::sub_year(const int year) {
    return year - 1900;
}

/**
 * Prints a header line that contains the month and year.
 * May not be the current month/year.
 * 
 * month is of pointer type char and is the month to print.
 * year is of type int and is the year to print.
*/
void calendar::print_month_year(const char * month, const int year, const bool leapYear) {
    cout << '\n' << END;
    for(int i = 0; i < LIN_C; ++i) cout << LIN;

    cout << SP << month << SP << year;
    if(leapYear) cout << '*';
    cout << SP;

    for(int i = 0; i < (leapYear ? LIN_C - 1 : LIN_C); ++i) cout << LIN;
    cout << END << endl;
}

/**
 * Prints a header line that contains the current time in hours, minutes and seconds.
 * 
 * hour is of type int and is the current hour.
 * min is of type int and is the current minute.
 * sec is of type int and is the current second.
*/
void calendar::print_time(const int hour, const int min, const int sec) {
    cout << END;
    for(int i = 0; i < LIN_C; ++i) cout << LIN;

    //Prints time as HH:mm:ss. Prepend/append spaces
    cout << SP;
    cout << (hour < 10 ? "0" : "") << hour << CL;
    cout << (min < 10 ? "0" : "") << min << CL;
    cout << (sec < 10 ? "0" : "") << sec << SP;

    for(int i = 0; i < LIN_C; ++i) cout << LIN;
    cout << END << endl;
}

/**
 * Prints a line of days from Sunday to Saturday
*/
void calendar::print_days() {
    cout << SP;
    for(const char * c : DAYS) cout << c << SP;
    cout << endl;
}

/**
 * Determines the first weekday (0 - 6) of the month.
 * 
 * currDay is of type int and is the current day of month (1 - 31).
 * daysSinceSunday is of type int and is the number of days since last Sunday (0 - 6).
 * return is of type int and is the first weekday (0 - 6) of the month.
*/
int calendar::first_of_month(const int currDay, const int daysSinceSunday) {
    //Determines the [day of month] of the first [day of week] of currDay
    int weekday = (currDay % DAYS_IN_WEEK) - 1;

    //Returns different calcluations depending on condition
    return weekday <= daysSinceSunday ? 
        daysSinceSunday - weekday : DAYS_IN_WEEK + daysSinceSunday - weekday;
}

/**
 * Prints the digits of a gregorian calendar.
 * 
 * firstOfMonth is of type int and is the first day (0 - 6) of the month.
 * currDay is of type int and is the current day of month (1 - 31).
 * daysInMonth is of type int and is the number of days in the month (1 - 31).
*/
void calendar::print_digits(
        const int firstOfMonth, const int currDay, const int daysInMonth, const bool indicate) {
    //Prevents the rest of the function from running if
    // [first of month] or [current day of month] is equal to or later
    // than the [days in current month]
    if(firstOfMonth >= daysInMonth || currDay > daysInMonth) {
        cout << "Cannot print out the calendar" << endl;
        return;
    }
    //Tabs over to align the day of month to day of week
    for(int i = 0; i < firstOfMonth * DIGITS_PER_DAY; ++i) cout << SP;

    int limiter = DAYS_IN_WEEK - firstOfMonth; //Initialize limiter
    int digit; //Keeps track of index for DIGITS array

    bool newLine = false; //New line flag to insert a new line within the for loop instead of after
    bool isCurrDigit; //Is true if the current pass in the loop is for the currDay digit
    bool isPrevDigit; //Is true if the previous pass in the loop was for the currDay digit

    int k = 1; //Limits the do/while loop below
    do {
        //Prints a certain number of digits, constrained by limit variable
        for(int l = 0; l < limiter; ++l) {
            //sets index to the sum of the indexes of the two loops
            digit = k + l;
            isCurrDigit = digit == currDay;
            isPrevDigit = digit - 1 == currDay;

            /* Need to satisfy these possibilities:
                >03 \n04
                <03> \n04
                 03\n 04
                 03\n 04<
                 03\n<04>
            */

            //New line if required
            if(newLine) {
                //Only prints a new line if indication is required
                // and if this pass is of the indicator digit
                // If not allos the other if statement to handle the new line
                if(indicate && isCurrDigit) {
                    cout << endl;
                    newLine = false; //Resets new line flag
                }
            }

            //Inserts the <##> indicator or space
            if(indicate && isCurrDigit) cout << IND_L; //'<'
            else if(indicate && isPrevDigit) cout << IND_R; //'>'
            else cout << SP; //' '

            //New line if required
            if(newLine) {
                cout << endl << SP; //Adds the space required at the beginning of the new line
                newLine = false; //Resets new line flag
            }

            //Prevents overflowing since the limiter will be set to
            // daysInMonth++ at the last do/while loop
            if(digit - 1 < daysInMonth) cout << calendar::DIGITS[digit - 1];
        }
        //Sets the new line flag
        newLine = true;

        //Adds printed digit count to loop index variable k
        k += limiter;
        //Determines the next loops digit count.
        // 7 weekdays or the remaining days in the month, whichever is less
        // If set to remaining days, adds a one to run the for loop one extra time
        //  This is to ensure '>' is added to the current day when it's the last of month
        limiter = min(DAYS_IN_WEEK, daysInMonth - digit + 1);
    } while(k <= daysInMonth); //Loops while index variable is less than total days in month

    //Double new line
    cout << '\n' << endl;
}

/**
 * Determines whether the current year/month requires an adjustment to the days of month.
 * 
 * month is of char pointer and is the month to print.
 * year is of type int and is the year to print.
 * daysInMonth is of type int and is the number of days in the current month.
*/
int calendar::adjust_for_lyear(const char * month, const int year, int daysInMonth) {
    //Declares pointer of type month_t, pointing to february pair
    const calendar::month_t * m = &calendar::MONTHS[1];
    //Declares a pointer of type char
    const char * mc = m->first;
    
    //Checks for leap year. Month must be february
    // All years which are perfectly divisible by 4 are leap years
    // except for century years (years ending with 00) which is leap year 
    // only it is perfectly divisible by 400.
    if(std::strncmp(mc, month, 3) == 0 && year % 4 == 0) {
        if(year % 100 == 0) {
            if(year % 400 == 0) return  ++daysInMonth;
            else return daysInMonth;
        } else return ++daysInMonth;
    } else return daysInMonth;
}

/**
 * Checks whether given year (from a tm struct) is a leap year.
 * 
 * tm_year is of type int and is the year value from a tm struct. Note, this value
 * isn't a Gregorian year value
 * 
 * Return is of type boolean and is true if the given year is a leap year.
*/
bool calendar::is_lyear(const int tm_year) {
    const int year = calendar::add_year(tm_year);

    //A leap year is defined as as follows:
    if(year % 4 == 0) {
        if(year % 100 == 0) {
            if(year % 400 == 0) return true;
            else return false;
        } else return true;
    } else return false;
}

/**
 * Determines if the day of month is within the days in a given month. Accounts for leap year
 * 
 * dayOfMonth is of type int and is the day of month to check
 * tm_struct is a pointer to struct of type tm that is used to determine the daysInMonth
 * 
 * Return is of type boolean and is true if dayOfMonth is within daysInMonth
*/
bool calendar::is_in_month(const int dayOfMonth, tm * tm_struct) {
    //checks for leap year
    int daysInMonth = MONTHS[tm_struct->tm_mon].second;
    if(calendar::is_lyear(tm_struct->tm_year) && tm_struct->tm_mon == 1) ++daysInMonth;

    return dayOfMonth > 0 && dayOfMonth <= daysInMonth;
}