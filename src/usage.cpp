#include "usage.hpp"

using std::cout;
using std::endl;
using std::left;
using std::right;

//Formatting lengths.
#define FORMAT_ARG_SIZE 3 //ARG_SIZE + 1
#define FORMAT_ARG_USE 14
#define FORMAT_EXPL 21

/**
 * Formats the pass argument and usage. If usage is pointing to null, prints
 * a blank string of the same length.
 * 
 * arg is of pointer type char and is the argument to format & print.
 * use is of pointer type char and is the usage of the argument to format & print.
*/
void usage::format_usage(const char * arg, const char * use) { 
    cout << right << std::setw(FORMAT_ARG_SIZE) << arg << ' ';

    if(use != nullptr) cout << left << std::setw(FORMAT_ARG_USE) << use << ' ';
    else cout << std::string(FORMAT_ARG_USE + 1, ' '); //c-string.
}

/**
 * Formats the explanation given to an argument. If indent is true, indents the
 * c-string to vertically match the non-indented lines.
 * 
 * indent is of type boolean and is true if the explanation requires an indent.
 * expl is of pointer type char and is the explanation associated to the argument.
*/
void usage::format_explanation(bool indent, const char * expl) {
    if(!indent) cout << left << ": " << expl << endl;
    else cout << std::string(FORMAT_EXPL, ' ') << expl << endl; //c-string.
}

/**
 * Prints the usage of this program
*/
void usage::print_usage() {
    cout << "Usage: calendar [option(s)]" << endl;
    cout << "Options: " << endl;
    
    usage::format_usage(ARG_HELP, nullptr);
    usage::format_explanation(false, "Prints out this message.");

    usage::format_usage(ARG_DAY, "[1 - 31]");
    usage::format_explanation(false, "Prints a calendar with an indicator on the provided day.");
    usage::format_explanation(true, "Combine with other options to view other months/years.");
    
    usage::format_usage(ARG_MONTH, "[1 - 12]");
    usage::format_explanation(false, "Prints a calendar with no indicator for the provided month.");
    usage::format_explanation(true, "Combine with other options to view other days/years.");

    usage::format_usage(ARG_YEAR, "[1900 - 9999]");
    usage::format_explanation(false, "Prints a calendar with no indicator for the provided year.");
    usage::format_explanation(true, "Combine with other options to view other days/months.");
}