#ifndef _USAGE_HPP
#define _USAGE_HPP

#include <iostream>
#include <iomanip>

//Size of arguments defined below
#define ARG_SIZE 2

//Arguments defined here
// used in main.cpp
#define ARG_DAY "-d"
#define ARG_MONTH "-m"
#define ARG_YEAR "-y"
#define ARG_HELP "-h"

//Declares namespace usage
// to prevent any possible variable/function duplication
namespace usage {

void format_usage(const char * arg, const char * use);
void format_explanation(bool indent, const char * expl);
void print_usage();

} //End namespace definition

#endif //End _USAGE_HPP if-block