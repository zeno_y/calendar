#ifndef _CALENDAR_HPP
#define _CALENDAR_HPP

#include <iostream>
#include <utility>
#include <cmath>
#include <cstring>
#include <ctime>

//Declares namespace calendar
// to prevent any possible variable/function duplication
namespace calendar {

//Define month_t type for ease of programming
// month_t is a pair of abbreviated month name and month length
typedef std::pair<const char *, const int> month_t;

extern const char * DAYS[];
extern const char * DIGITS[];

extern const month_t MONTHS[];

//Declare functions
int add_year(const int y_count);
int sub_year(const int year);

void print_month_year(const char * month, const int year, const bool leapYear);
void print_time(const int hour, const int min, const int sec);
void print_days();
int first_of_month(const int currDay, const int daysSinceSunday);
void print_digits(const int firstOfMonth, const int currDay, const int daysInMonth, const bool indicate);
int adjust_for_lyear(const char * month, const int year, int daysInMonth);
bool is_lyear(const int tm_year);
bool is_in_month(const int day, tm * tm_struct);

} //End namespace definition

#endif //End _CALENDAR_H if-block