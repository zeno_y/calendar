#
# Zeno Yuki - zeno.y@protonmail.com
# 12/13/2018
#

#Variable declarations
CC = g++
CPPX = cpp
OBJX = o
BIN_DIR = bin
SRC_DIR = src
OBJFLAG = -o
NLFLAG = -c #no link flag
CFLAGS = -Wextra -Wall -Werror -pedantic

BIN = calendar

OBJS := $(patsubst %.$(CPPX), %.$(OBJX), $(wildcard $(SRC_DIR)/*.$(CPPX)))

#Explicit dependency rules
%.$(OBJX): %.$(CPPX)
	@echo creating objects...
	$(CC) $(CFLAGS) $(NLFLAG) $(OBJFLAG) $@ $^

#Main Target
$(BIN): $(OBJS)
	@echo creating binary directory...
	mkdir -p $(BIN_DIR)
	@echo linking...
	$(CC) $(CFLAGS) $(OBJFLAG) $(BIN_DIR)/$@ $^

#Clean Standard Target
clean:
	@echo removing binary directory and object files...
	rm -rf $(BIN_DIR) $(SRC_DIR)/*.$(OBJX)